package com.stuller.mediators;

import java.io.*;
import java.net.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext; 
import org.apache.synapse.mediators.AbstractMediator;

/**
 * Should create a TCP connection using deviceName or IP , devicePort & socketTimeOut and send socketPayload to that socket
 * any data in the inputStream of the socket connection should be read and saved as tcpPayload property * 
 * 
 * basic requirements of the mediator:
 *** It should use a message context property as the payload holder for the robot
 *** It should use a message context property as the payload holder of the robot response
 *** It should have dynamic property for the robot name or IP
 *** It should have dynamic property for the robot port
 *** It should have dynamic property for the socket timeout value
 *** It should close the connection once the response from the robot is read
 *** It should continuously check for a response until the timeout value is reached before closing the connection
 * 
 * **** It only creates a connection when payload is available
 * @author GUser
 * @Date	14-08-2019
 * properties: socketPayload, deviceName, devicePort, socketTimeOut, tcpPayload
 * @param	context
 * @return	true
 */
public class TCPDispatchMediator extends AbstractMediator { 
	
	private static final Log log = LogFactory.getLog(TCPDispatchMediator.class);
	private String deviceName = null;
	private int devicePort = 0;	
	private int socketTimeOut = 2000;	
	
	public boolean mediate(MessageContext context) { 
		Socket socket = null;
		
		//get the message for the robot
		String msg = (String) context.getProperty("socketPayload");			
		if(msg != null) {
			try{
				//get the IP of the device if needed
				InetAddress addr = InetAddress.getByName(deviceName);
				//initialize the socket and make the connectin
			 	socket = new Socket();	  
			 	socket.connect(new InetSocketAddress(addr, devicePort), socketTimeOut);		 	
				// write message to socket
				PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			    writer.println(msg);
				log.info(  "WSO2EI" + " to " + deviceName + ": TX -- server TCPDispatchMediator: " + msg );
				
				//read response while socket is open
				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));	
				while (true){
					//break if socket closed
					if (socket.isClosed()) {
						log.info( "WSO2EI" + " to " + deviceName + ": - Server socket closed! TCPDispatchMediator -- ");
						break;
					}
					// read the line & break the loop			    
					String str;
					if((str = reader.readLine()) != null){
						context.setProperty("tcpPayload", str);
						log.info( "WSO2EI" + " to " + deviceName + ":  -  RX -- server TCPDispatchMediator: " + str);	 
						break;
					}				
				}
				writer.close();
				reader.close();
				socket.close();
				
			}catch (Exception e) {
				try {
					socket.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					log.error( "WSO2EI" + " to " + deviceName + ":  !!!!!! Exception e in TCPDispatchMediator.Mediate !!!!! " + e1.toString());
				}
				e.printStackTrace();
				log.error( "WSO2EI" + " to " + deviceName + ":  !!!!!! Exception e in TCPDispatchMediator.Mediate !!!!! " + e.toString());
			}
		}
		return true;
	}
	
	public String getType() {
	    return null;
	}
	 
    public void setTraceState(int traceState) {
        traceState = 0;
    }
 
    public int getTraceState() {
        return 0;
    }
    
    public void setDeviceName(String name) {
    	deviceName = name;
    }
    
    public String getDeviceName() {
    	return deviceName;
    }
    public void setDevicePort(int port) {
    	devicePort = port;
    }
    
    public int getDevicePort() {
    	return devicePort;
    } 
    public void setSocketTimeOut(int timeOut) {
    	socketTimeOut = timeOut;
    }
    
    public int getSocketTimeOut() {
    	return socketTimeOut;
    }    
}
